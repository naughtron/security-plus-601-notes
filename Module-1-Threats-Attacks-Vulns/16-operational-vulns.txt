operational vulns manifest in day-to-day activities and are in general
caused by lack of internal controls or weak configurations.

consequences include exposure, theft and loss, interruption of service, downtime,
resource exhaustion, and compromise.

Operational Vulns

Clear Text Credentials:
Issue: clear text, unencrypted or accessable creds.
Impact: captured / harvested creds = Unauthorized access.
Causes: Insecure protocols being used (ex: telnet), passwords being stored insecure,
passwords are being included in unencrypted compromise
Response:
- user educatoin
- password management
- multifactor authentication
- require secure protocols
- configuratoin and compliance auditing

Open Permissions:
Issue: Incorrect or missing permissions
Impact: Unauthorized access / access violations / DoS
Causes:
- provisioning process problem (permissions not requested or acknowledged)
- misconfiguration
- authorization creep
- excessive root or admin accounts.
Response:
- custodian education
- documented procedures
- access auditing
- access control list auditing
- group membership auditing
- log review and SIEM reports for violations.

Data Exfiltration:
Issue: inadvertant or intentional data exfiltration
Impact: Unauthorized discoloure reg or policy compliance violations
Causes:
- incorrect labeling or claffification
- user error (did not know not to share)
- malicious activity (APT)
- accidental
Response:
- user education
- log review and SIEM reports for violations
- DLP capture and alerts
- Review data classification controls
- Anti-Malware applicatoins
- Firewall rules

Open ports and services:
Issue: open ports and services
Impact:
- exposure and potential exploit
- unauthorized access, DoS, device integrity (administration)
Causes:
- poor configuration management
- not implimenting "least functionality"
- permission to install new software
Response:
- consistent confiruration and system hardening
- account restrictions
- vuln scanning to ID open porrts and services.

Weak patch management:
Issue: Weak patching
Impact: Exploits related to unpatched vulns
Causes:
- lax policies and procedures
- immature patch management program
- incomplete inventory of software and hardware
- vendor controlled systems
- narrow/non-existant maintenance windows
- concerns about impact.
Response:
- owner and custodian education
- approved patch management program
- reported metrics
- patch audit

Unauthorized software:
Issue: Installed software that has not been approved or authorized.
Impact:
- introduction of malware
- bandwidth utilization
- maintiance issues
- compatibility issues (vuln management)
- license violations
Causes:
- lack of centralized control
- local admin privs
- lack of standards and guadance
Response:
- User education
- port and vuln scanning
- asset inventory (audit)

Shadow IT:
Issue: use of cloud based applications and services that bypass
the IT department.
Impact: voliation of the orgs policies.
Causes:
- availablility of SaaS
- distrust / dislike of the IT dept.
- ability to bypass controls.
Response:
- user education
- Cloud Access Security Broker
- Firewall rules
- Log Review and SIEM reports for violations
- Accounting Audit.



