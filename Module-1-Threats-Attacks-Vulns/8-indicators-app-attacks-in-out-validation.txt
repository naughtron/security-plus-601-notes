Input validation: process of properly validating Input from client
or env.

Output validation: control what is returned to the screen.

Inputvalidation:
used to detect unauthorized input before it is processed by the appliation

Syntactic validation: enforce correct syntax of structured fields (example SS number format)

Semantic validation: enforce correctness of values (start date is before end date)

Input Validation Questoins:
Data format? - date, string, int, text
Appropriate size? - expected length.
Appropriate range? - num of days in a month, num of units, dollar value.
Type of data? - alpha, numeric, symbols
Right values? - start date/end date, future date

Input syntax validation approaches:

Block List: check that given data does not contain known bad content for example "<SCRIPT>".
The drawback is that this could be evaded by using lower case or mixed case.

Allow List: check that given data matches a set of known good rules. for example an allow list
for US states would be a field of 50 2-letter codes.

Client / Service side validation:

Server side validation takes place on the server.

Client side validation takes place in the browser.

- You can choose to do both, and when you do the validation on the server
acts as a second barrier that stops malicious bypass of client-side validation.

Preview examples input / output attacks:

Injection: tricking an application into including unintended commands in the data sent to an
interpriter.

XSS: injection of mal code into a vuln web application or back-end Db that will exe scripts in
users brower.

Cross Site Request Forgery CSRF/XSRF: Ticking the browser into exe a mal action on a trusted site
for which the user is currently authenticated. CSRF exploits the trust a sire has in a users browser.
